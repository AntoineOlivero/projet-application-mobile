package com.example.applicationceriproject;

import com.example.applicationceriproject.data.Element;

import java.util.HashMap;

public class ElementList {

    private static HashMap<String, Element> hashMap = init();

    private static HashMap<String, Element> init() {
        HashMap<String, Element> res = new HashMap<>();
        //res.put()
        return res;
    }

    public static String[] getNameArray() {
        return hashMap.keySet().toArray(new String[hashMap.size()]);
    }

    public static Element getElement(String id) {
        return hashMap.get(id);
    }

}
