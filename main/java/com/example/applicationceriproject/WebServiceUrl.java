package com.example.applicationceriproject;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    private static final String HOST = "www.demo-lia.univ-avignon.fr";

    private static final String PATHMUSEUM = "cerimuseum";
    private static final String PATHID = "ids";
    private static final String PATHCATEGORIES = "categories";
    private static final String PATHCATALOG = "catalog";
    private static final String PATHITEMS = "items";
    private static final String PATHTHUMBNAIL = "thumbnail";
    private static final String PATHIMAGES = "images";
    private static final String PATHDEMOS = "demos";


    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATHMUSEUM);
        return builder;
    }

    //retourne info sur une id particuliere
    private static final String SEARCH_ID = "searchId.php";
    public static URL buildSearchId(String valId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ID)
                .appendQueryParameter(PATHID, valId);
        URL url = new URL(builder.build().toString());
        System.out.println(url);
        return url;
    }

    //retourne info sur une catégorie
    private static final String SEARCH_CAT = "searchCat.php";
    public static URL buildSearchCategorie(String valCat) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CAT)
                .appendQueryParameter(PATHCATEGORIES, valCat);
        URL url = new URL(builder.build().toString());
        System.out.println(url);
        return url;
    }

    private static final String SEARCH_CATALOG = "searchCatalog.php";
    public static URL buildSearchCatalog(String valCatalog) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG)
                .appendQueryParameter(PATHCATALOG, valCatalog);
        URL url = new URL(builder.build().toString());
        System.out.println(url);
        return url;
    }



}
