package com.example.applicationceriproject.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class ElementDbHelper extends SQLiteOpenHelper {

    private static final String TAG = ElementDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "element.db";

    public static final String TABLE_NAME = "element";

    public static final String ID = "id";
    public static final String CATEGORIE = "categorie";
    public static final String NOM = "nom";
    public static final String DESCRIPTION = "description";
    public static final String TIMEFRAME = "timeframe";
    public static final String ANNEE = "annee";
    public static final String MARQUE = "marque";
    public static final String TECHNICALDETAIL = "technicalDetail";
    public static final String WORKING = "working";

    public static final String SQL_Delete = "Destruction de la table si déjà existante" + TABLE_NAME;

    public ElementDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void dropTable(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_Delete);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                ID + " TEXT PRIMARY KEY, " +
                CATEGORIE + " TEXT NOT NULL, " +
                NOM + " TEXT NOT NULL, " +
                DESCRIPTION + " TEXT, " +
                TIMEFRAME + " INTEGER, " +
                ANNEE + " INTEGER, " +
                MARQUE + " TEXT, " +
                TECHNICALDETAIL + " TEXT, " +
                WORKING + " BOOLEAN, " +

                "UNIQUE (" + ID + ")ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private ContentValues fill(Element element) {
        ContentValues values = new ContentValues();
        values.put(ID, element.getId());
        values.put(CATEGORIE, element.getCategories());
        values.put(NOM, element.getNom());
        values.put(DESCRIPTION, element.getDescription());
        values.put(TIMEFRAME, element.getTimeFrame());
        values.put(ANNEE, element.getAnnee());
        values.put(MARQUE, element.getMarque());
        values.put(TECHNICALDETAIL, element.getTechnicalDetail());
        values.put(WORKING, element.getWorking());

        return values;
    }

    public boolean addElement(Element element) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(element);

        Log.d(TAG, "adding: " + element.getNom() + " with id=" + element.getId());
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close();

        return (rowID != -1);
    }

    public Cursor fetchAllElements() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        Log.d(TAG, "call fetchAllElement()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }


}
