package com.example.applicationceriproject.data;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

public class Element implements Parcelable{


    public static final String TAG = Element.class.getSimpleName();

    private String id;
    private String categories;
    private String nom;
    private String description;
    private int timeFrame;
    private int annee;
    private String marque;
    private String technicalDetail;
    private boolean working;

    public Element() {

    }

    public Element(String id, String categories, String nom, String description,int timeFrame, int annee, String marque, String technicalDetail, boolean working) {

        this.id = id;
        this.categories = categories;
        this.nom = nom;
        this.description = description;
        this.timeFrame = timeFrame;
        this.annee = annee;
        this.marque = marque;
        this.timeFrame = timeFrame;
        this.working = working;
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    protected Element(Parcel in) {

        id = in.readString();
        categories = in.readString();
        nom = in.readString();
        description = in.readString();
        timeFrame = in.readInt();
        annee = in.readInt();
        marque = in.readString();
        technicalDetail = in.readString();
        working = in.readBoolean();
    }

    public static final Parcelable.Creator<Element> CREATOR = new Parcelable.Creator<Element>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public Element createFromParcel(Parcel in) {
            return new Element(in);
        }

        @Override
        public Element[] newArray(int size) {
            return new Element[size];
        }
    };

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getCategories() {
        return categories;
    }
    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public int getTimeFrame() {
        return timeFrame;
    }
    public void setTimeFrame(int timeFrame) {
        this.timeFrame = timeFrame;
    }

    public int getAnnee() {
        return annee;
    }
    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public String getMarque() {
        return marque;
    }
    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getTechnicalDetail() {
        return technicalDetail;
    }
    public void setTechnicalDetail(String technicalDetail) {
        this.technicalDetail = technicalDetail;
    }

    public Boolean getWorking() {
        return working;
    }
    public void setWorking(Boolean working) {
        this.working = working;
    }


    public  int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void writeToParcel(Parcel dest, int flag) {
        dest.writeString(id);
        dest.writeString(categories);
        dest.writeString(nom);
        dest.writeString(description);
        dest.writeLong(timeFrame);
        dest.writeLong(annee);
        dest.writeString(marque);
        dest.writeString(technicalDetail);
        dest.writeBoolean(working);
    }

}
