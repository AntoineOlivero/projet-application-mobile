package com.example.applicationceriproject;

import android.util.JsonReader;

import com.example.applicationceriproject.data.Element;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerElement {

    private static final String TAG = JSONResponseHandlerElement.class.getSimpleName();

    private Element element;

    public boolean exist = false;

    public JSONResponseHandlerElement(Element element) {
        this.element = element;
    }

    public void readJsonStream(InputStream ressponse) throws IOException {
        JsonReader reader = new JsonReader((new InputStreamReader(ressponse, "UTF-8")));
        try {
            readElement(reader);
        }finally {
            reader.close();
        }
    }
    public void readElement(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String id = reader.nextName();
            if (id.equals("id")) {
                readArrayId(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }

    private void readArrayId(JsonReader reader) throws IOException {

        reader.beginArray();
        int nb = 0;
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String valeur = reader.nextName();
                if (nb == 0) {
                    if (valeur.equals("id")) {
                        element.setId(reader.nextString());
                    } else if (valeur.equals("categorie")) {
                        element.setCategories(reader.nextString());
                    } else if (valeur.equals("nom")) {
                        element.setNom(reader.nextString());
                    } else if (valeur.equals("description")) {
                        element.setDescription(reader.nextString());
                    } else if (valeur.equals("timeFrame")) {
                        element.setTimeFrame(reader.nextInt());
                    } else if (valeur.equals("annee")) {
                        element.setAnnee(reader.nextInt());
                    } else if (valeur.equals("marque")) {
                        element.setMarque(reader.nextString());
                    } else if (valeur.equals("technicalDetail")) {
                        element.setTechnicalDetail(reader.nextString());
                    } else if (valeur.equals("working")) {
                        element.setWorking(reader.nextBoolean());
                    }
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
